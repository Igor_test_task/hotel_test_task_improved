<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel extends CI_Controller {

    private $login;
    private $password;

    private $hotelID;


    function __construct() {
        parent::__construct();

        $this->SetCredentials();
    }

    public function index()
    {
        $data = array(
            'hotelStayPackages' => $this->getHotelStayPackages(),
            'hotelInfo' => $this->getHotelInfo(),
            'hotelPhotos' => $this->getHotelPhoto(),
        );

        $this->load->view('list', $data);
    }

    private function getHotelStayPackages() {
        $request =
            '<?xml version="1.0"?>' . "\n" .
            '<request>' .
            '<login>' . htmlspecialchars($this->login) . '</login>' .
            '<password>' . htmlspecialchars($this->password) . '</password>' .
            '<hotId>' . $this->hotelID . '</hotId>' .
            '<lanId>3</lanId>' .
            '</request>';

        $ch = curl_init('http://api.unstable.cz/x1/hotel/getStayPackages');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return simplexml_load_string($response);
    }

    private function getHotelInfo() {
        $request =
            '<?xml version="1.0"?>' . "\n" .
            '<request>' .
            '<login>' . htmlspecialchars($this->login) . '</login>' .
            '<password>' . htmlspecialchars($this->password) . '</password>' .
            '<hotId>' . $this->hotelID . '</hotId>' .
            '</request>';

        $ch = curl_init('http://api.unstable.cz/x1/hotel/get');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return simplexml_load_string($response);
    }
    
    private function getHotelPhoto() {
        $request =
            '<?xml version="1.0"?>' . "\n" .
            '<request>' .
            '<login>' . htmlspecialchars($this->login) . '</login>' .
            '<password>' . htmlspecialchars($this->password) . '</password>' .
            '<hotId>' . $this->hotelID . '</hotId>' .
            '<lanId>3</lanId>' .
            '</request>';

        $ch = curl_init('http://api.unstable.cz/x1/hotel/getPhotogalleries');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return simplexml_load_string($response);
    }

    private function SetCredentials() {
        $this->login    = 'programmer@previo.cz';
        $this->password = 'pwd4programmer';

        $this->hotelID = 103;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
